FROM mcr.microsoft.com/dotnet/core/aspnet:3.0.0-preview6-alpine3.9 AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/core/sdk:3.0.100-preview6-alpine3.9 AS build
WORKDIR /src
COPY ["MatroskaServer/MatroskaServer.csproj", "MatroskaServer/"]
COPY ["Matroska/Matroska.csproj", "Matroska/"]
RUN dotnet restore "MatroskaServer/MatroskaServer.csproj"
COPY . .
WORKDIR "/src/MatroskaServer"
RUN dotnet build "MatroskaServer.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "MatroskaServer.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "MatroskaServer.dll"]