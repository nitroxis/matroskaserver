﻿using System;
using System.Buffers;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MatroskaServer
{
	/// <summary>
	/// Represents a HTTP chunked transfer encoding stream.
	/// </summary>
	public sealed class ChunkedStream : Stream
	{
		#region Fields
	
		private const int maxStackAlloc = 16 * 1024;

		private readonly Stream stream;
		private readonly bool leaveOpen;

		private int readPosition;
		private int readLength;

		private static readonly byte[] endChunk = {48, 13, 10, 13, 10}; // 0\r\n\r\n

		#endregion

		#region Properties
		
		public override bool CanRead => this.stream.CanRead;

		public override bool CanSeek => false;

		public override bool CanWrite => this.stream.CanWrite;

		public override long Length => throw new NotSupportedException();

		public override long Position
		{
			get => throw new NotSupportedException();
			set => throw new NotSupportedException();
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="ChunkedStream"/>.
		/// </summary>
		public ChunkedStream(Stream stream)
			: this(stream, false)
		{
			
		}

		/// <summary>
		/// Creates a new <see cref="ChunkedStream"/>.
		/// </summary>
		public ChunkedStream(Stream stream, bool leaveOpen)
		{
			this.stream = stream;
			this.leaveOpen = leaveOpen;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Writes an end chunk (length 0).
		/// </summary>
		private void writeEndChunk()
		{
			this.stream.Write(endChunk);
		}

		/// <summary>
		/// Writes an end chunk (length 0).
		/// </summary>
		private ValueTask writeEndChunkAsync()
		{
			return this.stream.WriteAsync(endChunk);
		}

		/// <summary>
		/// Reads the chunk header (length + newline).
		/// </summary>
		/// <param name="length"></param>
		/// <returns></returns>
		private bool readChunkHeader(out int length)
		{
			length = 0;

			Span<byte> header = stackalloc byte[16];
			int headerPos = 0;
			for (;;)
			{
				if (headerPos >= header.Length)
					throw new InvalidDataException("Chunk header too long.");

				int input = this.stream.ReadByte();
				if (input < 0)
				{
					if (header.Length > 0)
						throw new EndOfStreamException();

					return false;
				}

				header[headerPos++] = (byte)input;

				if (header.Length >= 2 && header[header.Length - 2] == '\r' && header[header.Length - 1] == '\n')
				{
					string lengthStr = Encoding.ASCII.GetString(header.Slice(0, headerPos));
					length = Convert.ToInt32(lengthStr, 16);

					return length > 0;
				}
			}
		}

		/// <summary>
		/// Reads the new-line at the end of a chunk.
		/// </summary>
		private void readChunkFooter()
		{
			// Read \r\n trail.
			int trail = this.stream.ReadByte();
			if (trail == -1)
				throw new EndOfStreamException();
			if (trail != 13)
				throw new InvalidDataException();

			trail = this.stream.ReadByte();
			if (trail == -1)
				throw new EndOfStreamException();
			if (trail != 10)
				throw new InvalidDataException();
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			if (buffer == null)
				throw new ArgumentNullException(nameof(buffer));

			if (offset < 0)
				throw new ArgumentOutOfRangeException(nameof(offset));
			
			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count));
			
			if (offset + count > buffer.Length)
				throw new ArgumentException("The sum of offset and count is greater than the buffer length.", nameof(count));

			// Check if need chunk is starting.
			if (this.readPosition >= this.readLength)
			{
				// Read footer of last chunk.
				if (this.readLength > 0)
					this.readChunkFooter();

				// Read new header.
				if (!this.readChunkHeader(out int len))
					return 0;

				this.readPosition = 0;
				this.readLength = len;
			}

			int available = this.readLength - this.readPosition;

			if (count > available)
				count = available;

			int n = this.stream.Read(buffer, offset, count);
			if (n == 0)
				throw new EndOfStreamException();

			this.readPosition += n;
			return n;
		}

		private static int getHexStringLength(int num)
		{
			if (num < 0)
				throw new ArgumentOutOfRangeException(nameof(num));

			int len = 0;

			while (num > 0)
			{
				++len;
				num >>= 4;
			}

			return len;
		}

		private static void intToHexString(int num, Span<byte> str)
		{
			int pos = str.Length - 1;

			while (pos >= 0)
			{
				int digit = num & 0xF;
				num >>= 4;

				if (digit < 10)
				{
					str[pos] = (byte)('0' + digit);
				}
				else
				{
					str[pos] = (byte)('A' + digit - 10);
				}

				--pos;
			}
		}

		public override void WriteByte(byte value)
		{
			Span<byte> data = stackalloc byte[6];
			data[0] = (byte)'1';
			data[1] = (byte)'\r';
			data[2] = (byte)'\n';
			data[3] = value;
			data[4] = (byte)'\r';
			data[5] = (byte)'\n';
			this.stream.Write(data);
		}

		private void writeStack(ReadOnlySpan<byte> buffer)
		{
			int count = buffer.Length;
			int headerLen = getHexStringLength(count);

			int totalSize = count + headerLen + 4;
			Span<byte> encoded = stackalloc byte[totalSize];

			intToHexString(count, encoded.Slice(0, headerLen));

			int pos = headerLen;
			encoded[pos++] = (byte)'\r';
			encoded[pos++] = (byte)'\n';

			buffer.CopyTo(encoded.Slice(pos, count));
			pos += count;

			encoded[pos++] = (byte)'\r';
			encoded[pos] = (byte)'\n';

			this.stream.Write(encoded);
		}

		private static IMemoryOwner<byte> encode(ReadOnlySpan<byte> buffer, out int totalSize)
		{
			int count = buffer.Length;
			int headerLen = getHexStringLength(count);

			totalSize = count + headerLen + 4;
			IMemoryOwner<byte> temp = MemoryPool<byte>.Shared.Rent(totalSize);
			Span<byte> encoded = temp.Memory.Span;

			intToHexString(count, encoded.Slice(0, headerLen));

			int pos = headerLen;
			encoded[pos++] = (byte)'\r';
			encoded[pos++] = (byte)'\n';

			buffer.CopyTo(encoded.Slice(pos, count));
			pos += count;

			encoded[pos++] = (byte)'\r';
			encoded[pos] = (byte)'\n';

			return temp;
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			if (buffer == null)
				throw new ArgumentNullException(nameof(buffer));

			if (offset < 0)
				throw new ArgumentOutOfRangeException(nameof(offset));
			
			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count));
			
			if (offset + count > buffer.Length)
				throw new ArgumentException("The sum of offset and count is greater than the buffer length.", nameof(count));

			if (count == 0)
				return;

			if (count <= maxStackAlloc)
			{
				this.writeStack(new Span<byte>(buffer, offset, count));
				return;
			}

			using IMemoryOwner<byte> temp = encode(new Span<byte>(buffer, offset, count), out int len);
			this.stream.Write(temp.Memory.Span.Slice(0, len));
		}

		public override void Write(ReadOnlySpan<byte> buffer)
		{
			if (buffer.Length <= maxStackAlloc)
			{
				this.writeStack(buffer);
				return;
			}

			using IMemoryOwner<byte> temp = encode(buffer, out int len);
			this.stream.Write(temp.Memory.Span.Slice(0, len));
		}

		public override async Task WriteAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
		{
			if (buffer == null)
				throw new ArgumentNullException(nameof(buffer));

			if (offset < 0)
				throw new ArgumentOutOfRangeException(nameof(offset));
			
			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count));
			
			if (offset + count > buffer.Length)
				throw new ArgumentException("The sum of offset and count is greater than the buffer length.", nameof(count));

			if (count == 0)
				return;

			using IMemoryOwner<byte> temp = encode(new Span<byte>(buffer, offset, count), out int len);
			await this.stream.WriteAsync(temp.Memory.Slice(0, len), cancellationToken);
		}

		public override async ValueTask WriteAsync(ReadOnlyMemory<byte> buffer, CancellationToken cancellationToken = new CancellationToken())
		{
			if (buffer.Length == 0)
				return;

			using IMemoryOwner<byte> temp = encode(buffer.Span, out int len);
			await this.stream.WriteAsync(temp.Memory.Slice(0, len), cancellationToken);
		}

		public override void Flush()
		{
			this.stream.Flush();
		}

		public override Task FlushAsync(CancellationToken cancellationToken)
		{
			return this.stream.FlushAsync(cancellationToken);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				this.Flush();
				this.writeEndChunk();
				this.stream.Flush();

				if (!this.leaveOpen)
					this.stream.Dispose();
			}
		}

		public override async ValueTask DisposeAsync()
		{
			await this.FlushAsync();
			await this.writeEndChunkAsync();
			await this.stream.FlushAsync();

			if (!this.leaveOpen)
				await this.stream.DisposeAsync();
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotSupportedException();
		}

		public override void SetLength(long value)
		{
			throw new NotSupportedException();
		}

		#endregion
	}
}
