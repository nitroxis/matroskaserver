﻿using Microsoft.AspNetCore.Http;

namespace MatroskaServer.Model
{
	public class PeerInfo
	{
		public ConnectionInfo Connection { get; }
		public string UserAgent { get; }

		public PeerInfo(ConnectionInfo connection, string userAgent)
		{
			this.Connection = connection;
			this.UserAgent = userAgent;
		}
	}
}