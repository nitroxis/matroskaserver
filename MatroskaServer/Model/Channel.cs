﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Matroska;
using Microsoft.AspNetCore.Connections;
using Microsoft.Extensions.Logging;

namespace MatroskaServer.Model
{

	/// <summary>
	/// Represents a channel.
	/// A channel has one peer uploading data in real-time and can have multiple <see cref="Viewer">Viewers</see> that download the data.
	/// </summary>
	[DataContract]
	public abstract class Channel : Peer
	{
		#region Fields

		private readonly ILogger logger;
		private readonly TaskCompletionSource<bool> initializationTCS;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the content type that was specified in the upload header.
		/// </summary>
		[DataMember]
		public string ContentType { get; }

		/// <summary>
		/// Gets the dataflow source for EBML data.
		/// </summary>
		public Source Source { get; private set; }

		/// <summary>
		/// Gets a task that can be awaited to wait until the channel becomes ready.
		/// </summary>
		public Task Initialization { get; }

		/// <summary>
		/// Gets the time code of the first cluster the viewer received.
		/// </summary>
		[DataMember]
		public TimeSpan StartTime { get; private set; }

		/// <summary>
		/// Gets the current timestamp.
		/// </summary>
		[DataMember]
		public TimeSpan Time { get; private set; }

		/// <summary>
		/// Gets the time code scale.
		/// </summary>
		public ulong TimecodeScale { get; private set; }

		/// <summary>
		/// Gets the track index of the video track.
		/// </summary>
		[DataMember]
		public int VideoTrackIndex { get; private set; }

		/// <summary>
		/// Gets the total amount of bytes received from the peer.
		/// </summary>
		[DataMember]
		public long BytesIn { get; private set; }

		/// <summary>
		/// Gets or sets the number of clusters processed.
		/// </summary>
		[DataMember]
		public int ClusterCount { get; private set; }

		/// <summary>
		/// Gets the latency (i.e. the offset between real time passed since creation and the stream time).
		/// </summary>
		[DataMember]
		public TimeSpan Latency => this.RealTime - (this.Time - this.StartTime);
		
		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="Channel"/>.
		/// </summary>
		protected Channel(ChannelInfo info, ILogger logger = null)
			: base(info)
		{
			this.ContentType = info.ContentType;
			this.logger = logger;

			this.initializationTCS = new TaskCompletionSource<bool>();
			this.Initialization = this.initializationTCS.Task;
			this.VideoTrackIndex = -1;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Creates a new viewer for this channel.
		/// </summary>
		/// <param name="info"></param>
		/// <returns></returns>
		public abstract Viewer CreateViewer(PeerInfo info);

		/// <summary>
		/// Returns all viewers currently watching this channel.
		/// </summary>
		/// <returns></returns>
		public abstract IEnumerable<Viewer> GetViewers();

		/// <summary>
		/// Run channel upload loop.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="pingInterval"></param>
		/// <returns></returns>
		public async Task Run(Stream stream, TimeSpan? pingInterval = null)
		{
			// Begin chunked HTTP stream.
			await using CountingStream countingStream = new CountingStream(stream, false);

			// Read EBML document.
			EbmlElement ebml = await (await EbmlElement.ReadElementAsync(countingStream)).CloneToMemoryAsync();
			if (ebml.ID != MatroskaSpecification.EBML.ID)
				throw new InvalidDataException();

			// Create and add channel.
			this.Source = new Source(ebml);
			this.BytesIn = countingStream.TotalBytesRead;
			this.TimecodeScale = 1000000;

			this.logger?.LogInformation("Source created.");
				
			// Set up ping.
			byte[] pingData = {13, 10};
			DateTime? nextPing = null;
			if (pingInterval != null)
			{
				nextPing = DateTime.Now;
			}

			bool gotInitialTime = false;

			try
			{
				// Read segments.
				await foreach (EbmlElement segment in EbmlElement.ReadElementsAsync(countingStream))
				{
					this.BytesIn = countingStream.TotalBytesRead;
					if (segment.ID != MatroskaSpecification.Segment.ID)
						throw new InvalidDataException("Expected segment element.");

					// This will be the processed segment element send to clients, constructed in-memory from the received elements.
					EbmlElement processedSegment = new EbmlElement(MatroskaSpecification.Segment.ID, -1, new MemoryStream());

					EbmlElement info = null;
					EbmlElement tracks = null;
					bool initialized = false;

					// Read segment elements.
					await foreach (EbmlElement element in EbmlElement.ReadElementsAsync(segment))
					{
						EbmlElement clone = await element.CloneToMemoryAsync();

						if (clone.ID != MatroskaSpecification.Cluster.ID)
						{
							// Debugging.
							await Utility.DumpMatroska(Console.Out, clone);
							clone.Position = 0;
						}

						if (clone.ID == MatroskaSpecification.Info.ID)
						{
							if (info != null)
								throw new InvalidDataException("Received duplicate info element.");

							// Get timecode scale.
							await foreach (EbmlElement infoElement in EbmlElement.ReadElementsAsync(clone))
							{
								if (infoElement.ID == MatroskaSpecification.TimecodeScale.ID)
								{
									this.TimecodeScale = infoElement.ReadUnsignedInteger();
								}
							}

							info = clone;
							info.CopyElement(processedSegment);
						}
						else if (clone.ID == MatroskaSpecification.Tracks.ID)
						{
							if (tracks != null)
								throw new InvalidDataException("Received duplicate tracks element.");

							this.VideoTrackIndex = Utility.FindVideoTrack(clone);

							tracks = clone;
							tracks.CopyElement(processedSegment);
						}
						else if (clone.ID == MatroskaSpecification.Cluster.ID)
						{
							// Extract properties.
							Utility.TryGetTimecode(clone, this.TimecodeScale, out TimeSpan time);
							this.Time = time;

							if (!gotInitialTime)
							{
								// Store initial time.
								this.StartTime = time;
								gotInitialTime = true;
							}

							this.logger?.LogDebug($"Received cluster with {clone.Length} bytes, timestamp: {time}. Broadcasting...");

							// Broadcast clusters to all targets.
							this.Source.Broadcast(clone);
							++this.ClusterCount;
						}
						else
						{
							// Discard.
							this.logger?.LogDebug($"Discarded element with ID {element.ID} ({MatroskaSpecification.Elements[element.ID]})");
						}

						if (!initialized && info != null && tracks != null)
						{
							this.Source.Segment = processedSegment;
							if (!this.initializationTCS.TrySetResult(true))
								break;

							initialized = true;
							this.logger?.LogInformation("Source initialized.");
						}

						if (nextPing != null && DateTime.Now > nextPing)
						{
							// Send a small ping every now and then to keep connections from timing out.
							// I don't think this is strictly allowed in HTTP, but it should work for nginx and the like.
							this.logger?.LogInformation("Ping");
							await stream.WriteAsync(pingData);
							await stream.FlushAsync();
							nextPing = DateTime.Now + pingInterval;
						}

						this.BytesIn = countingStream.TotalBytesRead;
					}
				}
			}
			catch (ConnectionResetException)
			{
				this.logger?.LogInformation("Connection reset.");
			}
			finally
			{
				// Complete dataflow block.
				this.Source.Complete();
				this.logger?.LogInformation("Source completed.");
			}
		}

		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
			this.initializationTCS.TrySetCanceled();
		}

		#endregion
	}
}
