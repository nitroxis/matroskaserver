﻿using Microsoft.AspNetCore.Http;

namespace MatroskaServer.Model
{
	public class ChannelInfo : PeerInfo
	{
		public string ContentType { get; }

		public ChannelInfo(ConnectionInfo connection, string userAgent, string contentType)
			: base(connection, userAgent)
		{
			this.ContentType = contentType;
		}
	}
}