﻿using System;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Http;

namespace MatroskaServer.Model
{
	/// <summary>
	/// Represents an abstract streaming peer.
	/// </summary>
	[DataContract]
	public abstract class Peer : IDisposable
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the creation time of this instance.
		/// </summary>
		[DataMember]
		public DateTime CreationTime { get; }

		/// <summary>
		/// Gets the connection info for the peer.
		/// </summary>
		public ConnectionInfo Connection { get; }
		
		/// <summary>
		/// Gets the connection ID.
		/// </summary>
		[DataMember]
		public string PeerId => this.Connection.Id;

		/// <summary>
		/// Gets the remote end-point name.
		/// </summary>
		[DataMember]
		public string RemoteEndPoint => $"{this.Connection.RemoteIpAddress}:{this.Connection.RemotePort}";

		/// <summary>
		/// Gets the peer's user agent.
		/// </summary>
		[DataMember]
		public string UserAgent { get; }

		/// <summary>
		/// Gets the real time passed since creation of the peer.
		/// </summary>
		[DataMember]
		public TimeSpan RealTime => DateTime.Now - this.CreationTime;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="Peer"/>.
		/// </summary>
		/// <param name="peerInfo"></param>
		protected Peer(PeerInfo peerInfo)
		{
			this.CreationTime = DateTime.Now;
			this.Connection = peerInfo.Connection;
			this.UserAgent = peerInfo.UserAgent;
		}

		#endregion

		#region Methods

		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
			}
		}

		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}
		
		#endregion
	}
}
