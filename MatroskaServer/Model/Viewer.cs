﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using Matroska;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace MatroskaServer.Model
{
	/// <summary>
	/// Represents a viewer of a <see cref="Channel"/>.
	/// </summary>
	[DataContract]
	public abstract class Viewer : Peer
	{
		#region Fields

		private readonly ILogger logger;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the channel the viewer is watching.
		/// </summary>
		public Channel Channel { get; }

		/// <summary>
		/// Gets the total amount of bytes sent to the peer.
		/// </summary>
		[DataMember]
		public long BytesOut { get; private set; }

		/// <summary>
		/// Gets the time code of the first cluster the viewer received.
		/// </summary>
		[DataMember]
		public TimeSpan StartTime { get; private set; }

		/// <summary>
		/// Gets the number of buffered (i.e. not yet sent) bytes.
		/// </summary>
		[DataMember]
		public TimeSpan Time { get; private set; }

		/// <summary>
		/// Gets the number of buffered (i.e. not yet sent) bytes.
		/// </summary>
		[DataMember]
		public TimeSpan Latency => this.Channel.Time - this.Time;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="Viewer"/>.
		/// </summary>
		protected Viewer(PeerInfo peerInfo, Channel channel, ILogger logger = null)
			: base(peerInfo)
		{
			this.Channel = channel ?? throw new ArgumentNullException(nameof(channel));
			this.logger = logger;
		}
		
		#endregion

		#region Methods

		public async Task Run(Stream stream, CancellationToken cancellationToken = default, bool waitForKeyFrame = true)
		{
			// Wait for initialization of the source.
			await this.Channel.Initialization;

			bool gotKeyFrame = this.Channel.VideoTrackIndex < 0;
			bool gotInitialTime = false;

			// Allow user to turn of waiting for a keyframe.
			if (!waitForKeyFrame)
				gotKeyFrame = true;

			await using CountingStream countingStream = new CountingStream(new ChunkedStream(stream, false), false);

			ActionBlock<EbmlElement> target = null;
			target = new ActionBlock<EbmlElement>(processElement, new ExecutionDataflowBlockOptions
			{
				MaxDegreeOfParallelism = 1,
				EnsureOrdered = true
			});
			
			async Task processElement(EbmlElement element)
			{
				TimeSpan time = TimeSpan.Zero;

				if (element.ID == MatroskaSpecification.Cluster.ID)
				{
					if (!gotKeyFrame)
					{
						using (element.KeepPosition())
						{
							if (!(await Utility.HasKeyFrameAsync(element, this.Channel.VideoTrackIndex)))
							{
								// We need a key frame first.
								this.logger.LogDebug("Waiting for keyframe...");
								return;
							}
						}

						gotKeyFrame = true;
						this.logger.LogInformation("Got keyframe.");
					}

					// Extract timestamp from element.
					using (element.KeepPosition())
					{
						Utility.TryGetTimecode(element, this.Channel.TimecodeScale, out time);
					}

					if (!gotInitialTime)
					{
						// Store initial time.
						this.StartTime = time;
						gotInitialTime = true;
					}
				}

				try
				{
					// Send element. This can block or take too long.
					await element.CopyElementAsync(countingStream, false, cancellationToken);
					this.logger.LogDebug($"Sent {element.Length} bytes ({MatroskaSpecification.Elements[element.ID]}), timestamp: {time}");

					// Store current time.
					this.Time = time;
				}
				catch (IOException ex)
				{
					this.logger.LogError($"Write error: {ex.Message}");
					target.Complete();
				}

				this.BytesOut = countingStream.TotalBytesWritten;
			}

			// Dataflow link.
			await using (cancellationToken.Register(target.Complete))
			using (this.Channel.Source.LinkTo(target, new DataflowLinkOptions
			{
				PropagateCompletion = true
			}))
			{
				this.logger.LogInformation("Target linked to source.");

				// Wait until the target completes.
				await target.Completion;

				this.logger.LogInformation("Target completed.");
			}
		}

		#endregion
	}
}
