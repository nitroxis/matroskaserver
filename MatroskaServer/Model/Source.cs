using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using Matroska;

namespace MatroskaServer.Model
{
	/// <summary>
	/// Represents a Matroska streaming dataflow source.
	/// </summary>
	public class Source : ISourceBlock<EbmlElement>
	{
		#region Fields

		private readonly TaskCompletionSource<bool> tcs;

		private EbmlElement segment;
		private long messageID;

		private readonly object mutex;
		private readonly List<Link> links;
		
		#endregion

		#region Properties

		/// <summary>
		/// Gets the EBML header.
		/// </summary>
		public EbmlElement Ebml { get; }
	
		/// <summary>
		/// Gets or sets the current Matroska "Segment" element which represents the header for a stream.
		/// </summary>
		public EbmlElement Segment
		{
			get
			{
				lock (this.mutex)
				{
					return this.segment?.CloneToMemory();
				}
			}
			set
			{
				lock (this.mutex)
				{
					this.segment = value?.CloneToMemory() ?? throw new ArgumentNullException(nameof(value));

					bool haveInfo = false;
					bool haveTracks = false;

					foreach (EbmlElement element in EbmlElement.ReadElements(this.segment))
					{
						if (element.ID == MatroskaSpecification.Info.ID)
						{
							haveInfo = true;
						}
						else if (element.ID == MatroskaSpecification.Tracks.ID)
						{
							haveTracks = true;
						}
					}

					if (!haveInfo)
						throw new ArgumentException("Info element is missing.", nameof(value));

					if (!haveTracks)
						throw new ArgumentException("Tracks element is missing.", nameof(value));

					// Send new segment to all receivers.
					foreach (Link link in this.links)
					{
						link.Target.Post(this.segment);
					}
				}
			}
		}

		/// <summary>
		/// Gets a <see cref="Task"/> that represents the asynchronous operation and completion of the dataflow block.
		/// </summary>
		public Task Completion => this.tcs.Task;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="Source"/> using the specified EBML header.
		/// </summary>
		/// <param name="ebml">The root EBML element.</param>
		public Source(EbmlElement ebml)
		{
			this.Ebml = ebml.CloneToMemory();
			this.mutex = new object();
			this.links = new List<Link>();
			this.messageID = 0;
			this.tcs = new TaskCompletionSource<bool>();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Broadcast the specified EBML element.
		/// </summary>
		/// <param name="cluster">The EBML element. Must be a <see cref="MatroskaSpecification.Cluster"/>.</param>
		public void Broadcast(EbmlElement cluster)
		{
			if (cluster.ID != MatroskaSpecification.Cluster.ID)
				throw new ArgumentException("Expected Cluster element.");

			Link[] linksCopy;

			lock (this.mutex)
			{
				if (this.segment == null)
					throw new InvalidOperationException("Channel is not initialized.");

				linksCopy = this.links.ToArray();
			}

			DataflowMessageHeader header = new DataflowMessageHeader(Interlocked.Increment(ref this.messageID));

			// Send cluster to all targets.
			foreach (Link link in linksCopy)
			{
				EbmlElement clone = cluster.CloneToMemory();

				// Offer to target.
				DataflowMessageStatus status = link.Target.OfferMessage(header, clone, this, false);
				switch (status)
				{
					case DataflowMessageStatus.Accepted:
						break;

					case DataflowMessageStatus.DecliningPermanently:
						this.unlink(link);
						break;

					default:
						Console.WriteLine($"A target returned {status}.");
						break;
				}
			}
		}

		/// <summary>
		/// Closes the channel and all its receivers.
		/// </summary>
		public void Complete()
		{
			lock (this.mutex)
			{
				foreach (Link link in this.links.ToArray())
				{
					if (link.Options.PropagateCompletion)
					{
						// Propagate completion.
						link.Target.Complete();
					}

					this.unlink(link);
				}

				Debug.Assert(this.links.Count == 0);
			}

			this.tcs.SetResult(true);
		}
		
		void IDataflowBlock.Fault(Exception exception)
		{
			lock (this.mutex)
			{
				foreach (Link link in this.links.ToArray())
				{
					if (link.Options.PropagateCompletion)
					{
						// Propagate fault.
						link.Target.Fault(exception);
					}

					this.unlink(link);
				}

				Debug.Assert(this.links.Count == 0);
			}

			this.tcs.SetException(exception);
		}
		
		/// <summary>
		/// Links the <see cref="Source"/> to the specified <see cref="ITargetBlock{EbmlElement}"/>.
		/// </summary>
		/// <param name="target"></param>
		/// <param name="linkOptions"></param>
		/// <returns></returns>
		public IDisposable LinkTo(ITargetBlock<EbmlElement> target, DataflowLinkOptions linkOptions)
		{
			Link link = new Link(this, target, linkOptions);

			// Send EBML to new target.
			if(!link.Target.Post(this.Ebml))
				throw new InvalidOperationException("Could not post EBML header to target.");
			
			lock (this.mutex)
			{
				// Send segment.
				if (this.segment != null)
				{
					if(!link.Target.Post(this.segment))
						throw new InvalidOperationException("Could not post segment to target.");
				}

				// Add to list.
				if (linkOptions.Append)
				{
					this.links.Add(link);
				}
				else
				{
					this.links.Insert(0, link);
				}
			}

			return link;
		}

		private void unlink(Link link)
		{
			lock (this.mutex)
			{
				this.links.Remove(link);
			}
		}

		EbmlElement ISourceBlock<EbmlElement>.ConsumeMessage(DataflowMessageHeader messageHeader, ITargetBlock<EbmlElement> target, out bool messageConsumed)
		{
			throw new NotSupportedException();
		}

		bool ISourceBlock<EbmlElement>.ReserveMessage(DataflowMessageHeader messageHeader, ITargetBlock<EbmlElement> target)
		{
			return false;
		}

		void ISourceBlock<EbmlElement>.ReleaseReservation(DataflowMessageHeader messageHeader, ITargetBlock<EbmlElement> target)
		{
			throw new NotSupportedException();
		}

		#endregion

		#region Nested Types

		/// <summary>
		/// Represents a Channel-Target link.
		/// </summary>
		private sealed class Link : IDisposable
		{
			private readonly Source channel;
			private int isDisposed;
			public ITargetBlock<EbmlElement> Target { get; }
			public DataflowLinkOptions Options { get; }

			public Link(Source channel, ITargetBlock<EbmlElement> target, DataflowLinkOptions options)
			{
				this.channel = channel;
				this.isDisposed = 0;
				this.Target = target;
				this.Options = options;
			}

			public void Dispose()
			{
				if (Interlocked.Increment(ref this.isDisposed) == 0)
					this.channel.unlink(this);
			}
		}

		#endregion
	}
}