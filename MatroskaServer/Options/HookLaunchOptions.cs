﻿namespace MatroskaServer.Options
{
	/// <summary>
	/// Options for a scripting hook.
	/// </summary>
	public sealed class HookLaunchOptions
	{
		public string FileName { get; set; }
		public string Arguments { get; set; }
	}
}
