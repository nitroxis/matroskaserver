namespace MatroskaServer.Options
{
	/// <summary>
	/// Options for channel hooks.
	/// </summary>
	public sealed class HookOptions
	{
		public HookLaunchOptions OnPostBegin { get; set; }
		public HookLaunchOptions OnPostEnd { get; set; }
		public HookLaunchOptions OnGetBegin { get; set; }
		public HookLaunchOptions OnGetEnd { get; set; }
	}
}