﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MatroskaServer.Options
{
	/// <summary>
	/// Channel upload/download options.
	/// </summary>
	public sealed class ChannelOptions
	{
		/// <summary>
		/// The amount of time to wait when a viewer tries to watch a channel that is currently offline in case it goes online in a few seconds.
		/// </summary>
		public TimeSpan? OfflineWait { get; set; } = TimeSpan.FromSeconds(5);

		/// <summary>
		/// The amount of time to wait between two consecutive pings to the streamer. These pings are just newlines sent to the HTTP client to circumvent timeout detection (in the client or a reverse proxy).
		/// </summary>
		public TimeSpan? PingInterval { get; set; } = TimeSpan.FromSeconds(10);
	}
}
