﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace MatroskaServer
{
	/// <summary>
	/// Represents a transparent stream that counts the number of bytes written to and read from it.
	/// </summary>
	public class CountingStream : Stream
	{
		#region Fields

		private readonly bool leaveOpen;
		private long totalBytesRead;
		private long totalBytesWritten;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the base stream.
		/// </summary>
		public Stream Stream { get; }

		/// <summary>
		/// Gets or sets the total number of bytes read from the stream.
		/// </summary>
		public long TotalBytesRead
		{
			get => this.totalBytesRead;
			set => this.totalBytesRead = value;
		}

		/// <summary>
		/// Gets or sets the total number of bytes written to the stream.
		/// </summary>
		public long TotalBytesWritten
		{
			get => this.totalBytesWritten;
			set => this.totalBytesWritten = value;
		}

		/// <inheritdoc />
		public override bool CanRead => this.Stream.CanRead;

		/// <inheritdoc />
		public override bool CanSeek => this.Stream.CanSeek;

		/// <inheritdoc />
		public override bool CanWrite => this.Stream.CanWrite;

		/// <inheritdoc />
		public override bool CanTimeout => this.Stream.CanTimeout;
		
		/// <inheritdoc />
		public override long Length => this.Stream.Length;

		/// <inheritdoc />
		public override long Position
		{
			get => this.Stream.Position;
			set => this.Stream.Position = value;
		}
		
		/// <inheritdoc />
		public override int ReadTimeout
		{
			get => this.Stream.ReadTimeout;
			set => this.Stream.ReadTimeout = value;
		}
		
		/// <inheritdoc />
		public override int WriteTimeout
		{
			get => this.Stream.WriteTimeout;
			set => this.Stream.WriteTimeout = value;
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new StreamCounter.
		/// </summary>
		public CountingStream(Stream stream, bool leaveOpen)
		{
			this.Stream = stream;
			this.leaveOpen = leaveOpen;
		}

		#endregion

		#region Methods

		/// <inheritdoc />
		public override void Flush()
		{
			this.Stream.Flush();
		}
		
		/// <inheritdoc />
		public override Task FlushAsync(CancellationToken cancellationToken)
		{
			return this.Stream.FlushAsync(cancellationToken);
		}

		/// <inheritdoc />
		public override long Seek(long offset, SeekOrigin origin)
		{
			return this.Stream.Seek(offset, origin);
		}
		
		/// <inheritdoc />
		public override void SetLength(long value)
		{
			this.Stream.SetLength(value);
		}

		/// <inheritdoc />
		public override int Read(byte[] buffer, int offset, int count)
		{
			int numBytes = this.Stream.Read(buffer, offset, count);
			Interlocked.Add(ref this.totalBytesRead, numBytes);
			return numBytes;
		}

		/// <inheritdoc />
		public override int ReadByte()
		{
			int result = this.Stream.ReadByte();
			if (result >= 0)
				Interlocked.Increment(ref this.totalBytesRead);
			return result;
		}

		/// <inheritdoc />
		public override async Task<int> ReadAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
		{
			int numBytes = await this.Stream.ReadAsync(buffer, offset, count, cancellationToken);
			Interlocked.Add(ref this.totalBytesRead, numBytes);
			return numBytes;
		}

		/// <inheritdoc />
		public override void Write(byte[] buffer, int offset, int count)
		{
			this.Stream.Write(buffer, offset, count);
			Interlocked.Add(ref this.totalBytesWritten, count);
		}

		/// <inheritdoc />
		public override void WriteByte(byte value)
		{
			this.Stream.WriteByte(value);
			Interlocked.Increment(ref this.totalBytesWritten);
		}

		/// <inheritdoc />
		public override async Task WriteAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
		{
			await this.Stream.WriteAsync(buffer, offset, count, cancellationToken);
			Interlocked.Add(ref this.totalBytesWritten, count);
		}

		/// <inheritdoc />
		protected override void Dispose(bool disposing)
		{
			if (disposing && !this.leaveOpen)
				this.Stream.Dispose();
		}

		public override async ValueTask DisposeAsync()
		{
			if (!this.leaveOpen)
				await this.Stream.DisposeAsync();
		}

		#endregion
	}
}
