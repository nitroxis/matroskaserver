﻿using System.Collections.Immutable;
using System.Diagnostics;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MatroskaServer.Model;
using MatroskaServer.Options;
using MatroskaServer.Services;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace MatroskaServer.Controllers
{
	/// <summary>
	/// Controller for streaming and viewing channels.
	/// </summary>
	[Route("")]
	public class ChannelController : Controller
	{
		#region Fields

		private readonly IChannelService channels;
		private readonly ChannelOptions channelOptions;
		private readonly HookOptions hooks;
		private readonly ILogger logger;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		public ChannelController(IChannelService channels, IOptions<HookOptions> hookOptions, IOptions<ChannelOptions> channelOptions, ILogger<ChannelController> logger)
		{
			this.channels = channels;
			this.channelOptions = channelOptions.Value;
			this.hooks = hookOptions.Value;
			this.logger = logger;
		}

		#endregion

		#region Methods

		private async Task<HttpStatusCode> runHook(HookLaunchOptions hook, string name, string password)
		{
			if (hook == null)
				return HttpStatusCode.OK;

			ProcessStartInfo startInfo = new ProcessStartInfo(hook.FileName, hook.Arguments);
			startInfo.Environment.Add("NAME", name);
			startInfo.Environment.Add("PASSWORD", password);
			Process process = Process.Start(startInfo);
			int code = await process.WaitForExitAsync();
			
			this.logger.LogInformation($"Hook returned {code}.");

			// Return status code based on exit code.
			if (code == 0)
				return HttpStatusCode.OK;

			if (code < 0)
				return HttpStatusCode.InternalServerError;

			return HttpStatusCode.Forbidden;
		}

		/// <summary>
		/// Returns a status page.
		/// </summary>
		/// <returns></returns>
		[HttpGet("status")]
		[HttpGet("status.json")]
		public ActionResult Status()
		{
			DefaultContractResolver contractResolver = new DefaultContractResolver
			{
				NamingStrategy = new CamelCaseNamingStrategy()
			};
			string str = JsonConvert.SerializeObject(this.channels.GetChannels().ToImmutableDictionary(), new JsonSerializerSettings()
			{
				ContractResolver = contractResolver
			});
			return this.Content(str, "application/json");
		}

		/// <summary>
		/// Disables HTTP buffering.
		/// </summary>
		private void disableBuffering()
		{
			IHttpResponseBodyFeature bufferingFeature = this.HttpContext.Features.Get<IHttpResponseBodyFeature>();
			bufferingFeature.DisableBuffering();
		}

		private async Task<Channel> getChannel(string name, bool waitForOnline)
		{
			CancellationTokenSource cts = CancellationTokenSource.CreateLinkedTokenSource(this.HttpContext.RequestAborted);

			if (!waitForOnline || this.channelOptions.OfflineWait == null)
				cts.Cancel();
			else
				cts.CancelAfter(this.channelOptions.OfflineWait.Value);

			return await this.channels.GetChannel(name, cts.Token);
		}

		[HttpGet("{channelName}")]
		[HttpHead("{channelName}")]
		public async Task Get(
			string channelName, 
			[FromQuery] bool waitForKeyFrame = true,
			[FromQuery] bool waitForOnline = true)
		{
			string clientName = $"{this.HttpContext.Connection.RemoteIpAddress}:{this.HttpContext.Connection.RemotePort}";
			using (this.logger.BeginScope($"{channelName}/{clientName}"))
			{
				try
				{
					this.Response.StatusCode = (int)await this.runHook(this.hooks.OnGetBegin, channelName, this.Request.Query["pw"]);
					if (this.Response.StatusCode >= 300)
						return;

					// Try to retrieve channel.
					Channel channel;

					try
					{
						channel = await this.getChannel(channelName, waitForOnline);
					}
					catch (TaskCanceledException)
					{
						this.Response.StatusCode = (int)HttpStatusCode.NotFound;
						return;
					}

					this.disableBuffering();

					PeerInfo info = new PeerInfo(this.HttpContext.Connection, this.Request.Headers[HeaderNames.UserAgent]);
					using (Viewer viewer = channel.CreateViewer(info))
					{
						this.logger.LogInformation($"Created viewer. User-Agent: \"{this.Request.Headers[HeaderNames.UserAgent]}\".");

						this.Response.ContentType = channel.ContentType ?? "video/x-matroska";
						this.Response.ContentLength = null;
						this.Response.Headers[HeaderNames.Connection] = "close"; // Close connection after streaming is over.
						this.Response.Headers[HeaderNames.CacheControl] = "no-store"; // Don't ever cache responses since they contain real-time live data.
						this.Response.Headers["X-Accel-Buffering"] = "no"; // For use behind nginx.

						if (this.Request.Method == "HEAD")
						{
							// Only the headers were requested.
							return;
						}

						this.Response.Headers[HeaderNames.TransferEncoding] = "chunked";

						await using var stream = new ProxyStream(this.Request.Body, this.Response.Body);
						await viewer.Run(stream, this.HttpContext.RequestAborted, waitForKeyFrame);
					}

					this.logger.LogInformation("Viewer removed.");
				}
				finally
				{
					await this.runHook(this.hooks.OnGetEnd, channelName, this.Request.Query["pw"]);
				}
			}
		}

		[HttpPost("{channelName}")]
		public async Task Post(string channelName)
		{
			using (this.logger.BeginScope(channelName))
			{
				try
				{
					this.Response.Headers[HeaderNames.Connection] = "close";
					this.Response.StatusCode = (int)await this.runHook(this.hooks.OnPostBegin, channelName, this.Request.Query["pw"]);
					if (this.Response.StatusCode >= 300)
						return;

					this.disableBuffering();

					// Create and try to add channel.
					ChannelInfo info = new ChannelInfo(this.HttpContext.Connection, this.Request.Headers[HeaderNames.UserAgent], this.Request.Headers[HeaderNames.ContentType].ToString());
					if (!this.channels.TryCreateChannel(channelName, info, out Channel channel))
					{
						this.Response.StatusCode = (int)HttpStatusCode.Forbidden;
						return;
					}

					this.logger.LogInformation("Channel added.");

					using (channel)
					{
						this.logger.LogInformation($"Receiving from {this.HttpContext.Connection.RemoteIpAddress}:{this.HttpContext.Connection.RemotePort}. User-Agent: \"{this.Request.Headers[HeaderNames.UserAgent]}\".");
						this.disableBuffering();

						await using var stream = new ProxyStream(this.Request.Body, this.Response.Body);
						await channel.Run(stream, this.channelOptions.PingInterval);
					}

					this.logger.LogInformation("Channel removed.");
				}
				finally
				{
					await this.runHook(this.hooks.OnPostEnd, channelName, this.Request.Query["pw"]);
				}
			}
		}

		#endregion
	}
}
