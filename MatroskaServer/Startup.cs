﻿using MatroskaServer.Model;
using MatroskaServer.Options;
using MatroskaServer.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json.Serialization;

namespace MatroskaServer
{
	public class Startup
	{
		#region Fields

		private readonly IConfiguration config;

		#endregion

		#region Constructors

		public Startup(IConfiguration config)
		{
			this.config = config;
		}

		#endregion

		#region Methods

		public void ConfigureServices(IServiceCollection services)
		{
			services.Configure<ForwardedHeadersOptions>(options =>
			{
				options.ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
			});

			services.AddOptions();
			services.Configure<HookOptions>(this.config.GetSection("Hooks"));
			services.Configure<ChannelOptions>(this.config.GetSection("Channels"));
			services.AddSingleton<IChannelService, ConcurrentChannelService>();
			services.AddMvc();
		}

		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			app.UseForwardedHeaders();

			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseRouting();
			
			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});
		}

		#endregion
	}
}
