﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;
using MatroskaServer.Model;
using Microsoft.Extensions.Logging;

namespace MatroskaServer.Services
{
	public class ConcurrentChannelService : IChannelService
	{
		#region Fields

		private readonly ILogger logger;
		private readonly ConcurrentDictionary<string, ChannelSlot> channels;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		public ConcurrentChannelService(ILogger<ConcurrentChannelService> logger)
		{
			this.logger = logger;
			this.channels = new ConcurrentDictionary<string, ChannelSlot>();
		}

		#endregion

		#region Methods

		public IEnumerable<KeyValuePair<string, Channel>> GetChannels()
		{
			return this.channels
				.Select(p => new KeyValuePair<string, Channel>(p.Key, p.Value.GetChannel()))
				.Where(p => p.Value != null);
		}

		public async ValueTask<Channel> GetChannel(string name, CancellationToken cancellationToken = default)
		{
			ChannelSlot slot = this.channels.GetOrAdd(name, _ => new ChannelSlot());
			return await slot.GetChannelAsync(cancellationToken);
		}
		
		public bool TryCreateChannel(string name, ChannelInfo info, out Channel channel)
		{
			channel = null;

			SimpleChannel ch = new SimpleChannel(name, this, info);

			this.channels.AddOrUpdate(name,
				_ => new ChannelSlot(ch),
				(k, v) =>
				{
					if (!v.TrySetChannel(ch))
						ch = null;

					return v;
				});
			
			channel = ch;
			return ch != null;
		}
		
		#endregion

		#region Nested Types

		/// <summary>
		/// Implements an asynchronously retrievable channel slot.
		/// </summary>
		private sealed class ChannelSlot
		{
			private SimpleChannel channel;
			private List<TaskCompletionSource<SimpleChannel>> waitList;

			public ChannelSlot()
			{

			}

			public ChannelSlot(SimpleChannel channel)
			{
				this.channel = channel;
			}

			public bool TrySetChannel(SimpleChannel ch)
			{
				if (ch == null)
					throw new ArgumentNullException(nameof(ch));

				List<TaskCompletionSource<SimpleChannel>> waitListCopy;

				lock (this)
				{
					if (this.channel != null)
						return false;

					this.channel = ch;

					waitListCopy = this.waitList;
					this.waitList = null;
				}

				if (waitListCopy != null)
				{
					foreach (TaskCompletionSource<SimpleChannel> tcs in waitListCopy)
					{
						tcs.TrySetResult(ch);
					}
				}

				return true;
			}

			public SimpleChannel GetChannel()
			{
				return this.channel;
			}

			public async ValueTask<SimpleChannel> GetChannelAsync(CancellationToken cancellationToken)
			{
				TaskCompletionSource<SimpleChannel> tcs;

				lock (this)
				{
					if (this.channel != null)
						return this.channel;

					cancellationToken.ThrowIfCancellationRequested();

					if (this.waitList == null)
						this.waitList = new List<TaskCompletionSource<SimpleChannel>>();

					tcs = new TaskCompletionSource<SimpleChannel>();
					cancellationToken.Register(() => tcs.TrySetCanceled());

					this.waitList.Add(tcs);
				}

				return await tcs.Task;
			}
		}

		/// <summary>
		/// Simple <see cref="Channel"/> implementation.
		/// </summary>
		[DataContract]
		private sealed class SimpleChannel : Channel
		{
			private readonly string name;
			private readonly ConcurrentChannelService service;
			private readonly object viewerMutex;
			private readonly List<SimpleViewer> viewers;

			[DataMember]
			public ImmutableArray<SimpleViewer> Viewers
			{
				get
				{
					lock (this.viewerMutex)
					{
						return this.viewers.ToImmutableArray();
					}
				}
			}
			
			public SimpleChannel(string name, ConcurrentChannelService service, ChannelInfo info)
				: base(info, service.logger)
			{
				this.name = name;
				this.service = service;
				this.viewers = new List<SimpleViewer>();
				this.viewerMutex = new object();
			}

			public override Viewer CreateViewer(PeerInfo info)
			{
				SimpleViewer viewer = new SimpleViewer(info, this, this.service.logger);

				lock (this.viewerMutex)
				{
					this.viewers.Add(viewer);
				}

				return viewer;
			}

			public override IEnumerable<Viewer> GetViewers()
			{
				lock (this.viewerMutex)
				{
					return this.viewers.ToArray();
				}
			}

			public void RemoveViewer(SimpleViewer viewer)
			{
				bool res; 

				lock (this.viewerMutex)
				{
					res = this.viewers.Remove(viewer);
				}

				if (!res)
				{
					this.service?.logger.LogError($"Failed to remove viewer {viewer.Connection.RemoteIpAddress}:{viewer.Connection.RemotePort} from channel \"{this.name}\".");
				}
			}

			protected override void Dispose(bool disposing)
			{
				base.Dispose(disposing);

				if (disposing)
				{
					if (!this.service.channels.TryRemove(this.name, out ChannelSlot slot))
					{
						this.service?.logger.LogError($"Failed to remove channel \"{this.name}\" from channel dictionary.");
					}

					if (!ReferenceEquals(slot.GetChannel(), this))
					{
						this.service?.logger.LogError($"The channel \"{this.name}\" that was removed from the channel dictionary is not the same as the current instance.");
					}
				}
			}
		}

		/// <summary>
		/// Implements a <see cref="Viewer"/> for <see cref="SimpleChannel"/>.
		/// </summary>
		[DataContract]
		private sealed class SimpleViewer : Viewer
		{
			private readonly SimpleChannel channel;

			public SimpleViewer(PeerInfo peerInfo, SimpleChannel channel, ILogger logger = null) 
				: base(peerInfo, channel, logger)
			{
				this.channel = channel;
			}

			protected override void Dispose(bool disposing)
			{
				base.Dispose(disposing);

				if (disposing)
				{
					this.channel.RemoveViewer(this);
				}
			}
		}

		#endregion
	}
}
