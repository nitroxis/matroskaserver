﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MatroskaServer.Model;

namespace MatroskaServer.Services
{
	public interface IChannelService
	{
		IEnumerable<KeyValuePair<string, Channel>> GetChannels();

		ValueTask<Channel> GetChannel(string name, CancellationToken cancellationToken = default);

		bool TryCreateChannel(string name, ChannelInfo info, out Channel channel);
	}
}
