﻿using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace MatroskaServer
{
	public class Program
	{
		public static void Main(string[] args)
		{
			CreateWebHostBuilder(args).Build().Run();
		}

		public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
			WebHost.CreateDefaultBuilder(args)
				.ConfigureAppConfiguration((hostingContext, config) =>
				{
					config.SetBasePath(Directory.GetCurrentDirectory());
					config.AddJsonFile("hosting.json", optional: true, reloadOnChange: true);
					config.AddJsonFile("settings.json", optional: true, reloadOnChange: true);
					config.AddEnvironmentVariables();
					config.AddCommandLine(args);
				})
				.ConfigureKestrel(options =>
				{
					options.Limits.MaxRequestBodySize = null;
					options.Limits.MinRequestBodyDataRate = null;
				})
				.UseStartup<Startup>();
	}
}
