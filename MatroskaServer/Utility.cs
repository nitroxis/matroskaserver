﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Matroska;

namespace MatroskaServer
{
	/// <summary>
	/// Utility functions.
	/// </summary>
	public static class Utility
	{	
		#region Methods

		public static async Task<int> WaitForExitAsync(this Process proc, CancellationToken cancellationToken = default)
		{
			TaskCompletionSource<int> tcs = new TaskCompletionSource<int>();

			using (cancellationToken.Register(() => { tcs.TrySetCanceled(); }))
			{
				void onExited(object sender, EventArgs e)
				{
					tcs.TrySetResult(proc.ExitCode);
				}

				proc.EnableRaisingEvents = true;
				proc.Exited += onExited;

				try
				{
					cancellationToken.ThrowIfCancellationRequested();
					if (proc.HasExited)
					{
						return proc.ExitCode;
					}

					return await tcs.Task;
				}
				finally
				{
					proc.Exited -= onExited;

				}
			}
		}


		/// <summary>
		/// Checks if the specified cluster contains a key frame for the specified track index.
		/// </summary>
		/// <param name="cluster"></param>
		/// <param name="track"></param>
		/// <returns></returns>
		public static async Task<bool> HasKeyFrameAsync(EbmlElement cluster, int track)
		{
			await foreach (EbmlElement simpleBlock in EbmlElement.ReadElementsAsync(cluster))
			{
				if (simpleBlock.ID != MatroskaSpecification.SimpleBlock.ID)
					continue;

				int index = simpleBlock.ReadByte();

				if ((index & 0x80) == 0)
				{
					index = (index << 7) | simpleBlock.ReadByte();
				}
				else
				{
					index = index & 0x7F;
				}

				if (index == track)
				{
					simpleBlock.ReadByte();
					simpleBlock.ReadByte();

					int flags = simpleBlock.ReadByte();
					return (flags & 0x80) != 0;
				}
			}

			return false;
		}

		/// <summary>
		/// Tries to extract the time code from a cluster.
		/// </summary>
		/// <param name="cluster"></param>
		/// <param name="scale"></param>
		/// <param name="timecode"></param>
		/// <returns></returns>
		public static bool TryGetTimecode(EbmlElement cluster, ulong scale, out TimeSpan timecode)
		{
			foreach (EbmlElement clusterElement in EbmlElement.ReadElements(cluster))
			{
				if (clusterElement.ID == MatroskaSpecification.Timecode.ID)
				{
					// Update channel timestamp.
					ulong tc = clusterElement.ReadUnsignedInteger();
					ulong ticks = tc * scale / 100;
					timecode = TimeSpan.FromTicks((long)ticks);
					return true;
				}
			}

			timecode = default;
			return false;
		}

		/// <summary>
		/// Returns the index of the video track in the specified "Tracks" element.
		/// </summary>
		/// <param name="tracks"></param>
		/// <returns></returns>
		public static int FindVideoTrack(EbmlElement tracks)
		{
			if (tracks.ID != MatroskaSpecification.Tracks.ID)
				throw new ArgumentException("Expected \"Tracks\" element.", nameof(tracks));

			foreach (EbmlElement trackEntry in EbmlElement.ReadElements(tracks).Where(e => e.ID == MatroskaSpecification.TrackEntry.ID))
			{
				int trackNumber = -1;
				int trackType = -1;

				// Find video track.
				foreach (EbmlElement property in EbmlElement.ReadElements(trackEntry))
				{
					if (property.ID == MatroskaSpecification.TrackNumber.ID)
					{
						trackNumber = (int)property.ReadUnsignedInteger();
					}
					else if (property.ID == MatroskaSpecification.TrackType.ID)
					{
						trackType = (int)property.ReadUnsignedInteger();
					}
				}

				if (trackType == 1) // Video.
				{
					// Store video track index so clients can properly wait for keyframes.
					return trackNumber;
				}
			}

			return -1;
		}

		private static async Task dump(TextWriter writer, EbmlElement element, int level)
		{
			writer.Write(new string('\t', level));

			writer.Write($"[{element.ID:X}:{element.Length}]");

			if (MatroskaSpecification.Elements.TryGetValue(element.ID, out EbmlElementInfo info))
			{
				writer.Write($" '{info.Name}' {info.Type}");

				element.Position = 0;

				switch (info.Type)
				{
					case EbmlElementType.SignedInteger:
						writer.WriteLine($" = {await element.ReadSignedIntegerAsync()}");
						break;
					case EbmlElementType.UnsignedInteger:
						writer.WriteLine($" = {await element.ReadUnsignedIntegerAsync()}");
						break;
					case EbmlElementType.Float:
						writer.WriteLine($" = {await element.ReadFloatAsync()}");
						break;
					case EbmlElementType.String:
						writer.WriteLine($" = {await element.ReadStringAsync()}");
						break;
					case EbmlElementType.UTF8:
						writer.WriteLine($" = {await element.ReadUTF8Async()}");
						break;
					case EbmlElementType.Date:
						writer.WriteLine($" = {await element.ReadDateAsync()}");
						break;
					case EbmlElementType.Master:
						writer.WriteLine(":");
						await foreach (EbmlElement child in EbmlElement.ReadElementsAsync(element))
						{
							await dump(writer, child, level + 1);
						}

						break;
					case EbmlElementType.Binary:
						byte[] temp = new byte[1];
						int count = 0;
						string indent = new string('\t', level + 1);
						writer.Write(":");
						while ((await element.ReadAsync(temp, 0, 1)) != 0)
						{
							if (count++ % 16 == 0)
							{
								writer.WriteLine();
								writer.Write(indent);
							}

							writer.Write($"{temp[0]:X2} ");
						}

						writer.WriteLine();

						break;
					default:
						writer.WriteLine();
						break;
				}
			}
			else
			{
				writer.WriteLine(" ?");
			}
		}

		public static async Task DumpMatroska(TextWriter writer, EbmlElement element)
		{
			await dump(writer, element, 0);
		}

		public struct StreamPositionStore : IDisposable
		{
			private readonly Stream stream;
			private long position;

			public StreamPositionStore(Stream stream, long position)
			{
				this.stream = stream;
				this.position = position;
			}

			public void Dispose()
			{
				if (this.position >= 0)
				{
					this.stream.Position = this.position;
					this.position = -1;
				}
			}
		}

		public static StreamPositionStore KeepPosition(this Stream stream)
		{
			return new StreamPositionStore(stream, stream.Position);
		}

		#endregion
	}
}
